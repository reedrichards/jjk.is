Welcome page for the [jjk.is](https://jjk.is) domain. 

Currently lists public services hosted on the domain.

npm install --legacy-peer-deps

gitpod /workspace/jjk.is (master) $ export NIXPKGS_ALLOW_INSECURE=1
gitpod /workspace/jjk.is (master) $ nix develop --impure