import React from "react";
import { Link } from "gatsby";

export default () => (
  <>
    <nav>
      <ul
        style={{
          listStyle: "none",
          paddingInlineStart: "0px",
          display: "inline-flex"
        }}
      >
        <li style={{ paddingRight: "15px" }}>
          <Link to="/">Home</Link>
        </li>

        <li style={{ paddingRight: "15px" }}>
          <Link to="/nt">New Tab Page</Link>
        </li>

      </ul>
    </nav>
    <hr></hr>
  </>
);
